### type_ahead

App to show type ahead functionality in a text form field.

All the main app logic is inside CityFinderBloc. Communication between UI and CityFinderBloc is done via events.
There's a debounce timer to prevent making unneeded requests while the user is typing.
The infrastructure simulates a 2 seconds delay while making the request.
The overlay UI logic is inside TypeAheadFormField.
While the request is being fetch, a loader (made by me) is shown.



Video:
[Preview](https://drive.google.com/uc?id=12gTEm68MuKXBRSNUnBMuxvkkrtF8IEtR)