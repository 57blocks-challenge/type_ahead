part of 'city_finder_bloc.dart';

@immutable
abstract class CityFinderEvent {
  const CityFinderEvent();
}

class CityTextChanged extends CityFinderEvent {
  final String _cityText;

  const CityTextChanged(this._cityText) : super();
}
