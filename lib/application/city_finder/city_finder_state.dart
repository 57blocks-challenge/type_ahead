part of 'city_finder_bloc.dart';

@immutable
class CityFinderState {
  final List<String> _suggestedCities;
  final bool isLoading;
  final bool showOverlay;

  List<String> get suggestedCities => List.unmodifiable(_suggestedCities);

  CityFinderState(
      {actualCityText = "",
      List<String> suggestedCities = const [],
      this.isLoading = false,
      this.showOverlay = false})
      : _suggestedCities = suggestedCities;
}

class CityFinderInitial extends CityFinderState {}
