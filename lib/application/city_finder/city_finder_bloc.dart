import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';
import 'package:type_ahead/infrastructure/cities/city_repository.dart';

part 'city_finder_event.dart';
part 'city_finder_state.dart';

class CityFinderBloc extends Bloc<CityFinderEvent, CityFinderState> {
  final CityRepository _cityRepository;

  CityFinderBloc({CityRepository cityRepository})
      : _cityRepository = cityRepository,
        super(CityFinderInitial());

  // Debounce text change events to prevent spamming the server while typing
  @override
  Stream<Transition<CityFinderEvent, CityFinderState>> transformEvents(
      Stream<CityFinderEvent> events, transitionFn) {
    final nonDebounceStream =
        events.where((event) => event is! CityTextChanged);
    final debounceStream = events
        .where((event) => event is CityTextChanged)
        .debounceTime(Duration(milliseconds: 500));

    return super.transformEvents(
        MergeStream([nonDebounceStream, debounceStream]), transitionFn);
  }

  @override
  Stream<CityFinderState> mapEventToState(
    CityFinderEvent event,
  ) async* {
    if (event is CityTextChanged) {
      yield* _mapCityTextChangedToState(event);
    }
  }

  Stream<CityFinderState> _mapCityTextChangedToState(
      CityTextChanged event) async* {
    final actualCityText = event._cityText;

    final isLoading = actualCityText.isNotEmpty;
    final showOverlay = isLoading;

    yield CityFinderState(
        actualCityText: actualCityText,
        isLoading: isLoading,
        showOverlay: showOverlay,
        suggestedCities: []);

    if (!isLoading) {
      return;
    }

    final cities = await _cityRepository.getCitiesStartingWith(actualCityText);

    yield CityFinderState(
        actualCityText: actualCityText,
        isLoading: false,
        showOverlay: cities.length > 0,
        suggestedCities: cities);
  }
}
