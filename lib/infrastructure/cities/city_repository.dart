const cities = [
  "Arizona",
  "Arkansas",
  "Armenia",
  "Cali",
  "California",
  "Medellín"
];

class CityRepository {
  Future<List<String>> getCitiesStartingWith(String cityStart) async {
    // Simulate request delay
    await Future.delayed(Duration(seconds: 2));
    return cities
        .where((element) =>
            element.toLowerCase().startsWith(cityStart.toLowerCase()))
        .toList();
  }
}
