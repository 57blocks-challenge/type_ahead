import 'package:flutter/material.dart';
import 'package:type_ahead/presentation/core/widgets/points_loader.dart';

class TypeAheadFormField extends StatefulWidget {
  final bool _showOverlay;
  final bool _isLoading;
  final List<String> _suggestedCities;
  final String _labelText;
  final void Function(String newText) _onTextChanged;

  TypeAheadFormField({
    @required void Function(String newText) onTextChanged,
    @required bool showOverlay,
    @required bool isLoading,
    @required List<String> suggestedCities,
    String labelText = "",
    Key key,
  })  : _onTextChanged = onTextChanged,
        _showOverlay = showOverlay,
        _isLoading = isLoading,
        _suggestedCities = suggestedCities,
        _labelText = labelText,
        super(key: key);

  @override
  _TypeAheadFormFieldState createState() => _TypeAheadFormFieldState();
}

class _TypeAheadFormFieldState extends State<TypeAheadFormField> {
  OverlayEntry _overlayEntry;

  @override
  Widget build(BuildContext context) {
    if (widget._showOverlay) {
      _showOverlay();
    } else {
      _hideOverlay();
    }

    return TextFormField(
        decoration: InputDecoration(labelText: widget._labelText),
        onChanged: widget._onTextChanged);
  }

  _showOverlay() {
    if (_overlayEntry != null) {
      _hideOverlay();
    }
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _overlayEntry = _createOverlayEntry(context);
      Overlay.of(context).insert(_overlayEntry);
    });
  }

  _hideOverlay() {
    if (_overlayEntry != null) {
      _overlayEntry.remove();
      _overlayEntry = null;
    }
  }

  OverlayEntry _createOverlayEntry(BuildContext context) {
    RenderBox renderBox = context.findRenderObject();
    final Size size = renderBox.size;
    final Offset offset = renderBox.localToGlobal(Offset.zero);

    return OverlayEntry(
        builder: (context) => Positioned(
              left: offset.dx,
              top: offset.dy + size.height + 5,
              width: size.width,
              child: Material(
                elevation: 4,
                child: widget._isLoading
                    ? PointsLoader.random()
                    : ListView.builder(
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                        shrinkWrap: true,
                        itemCount: widget._suggestedCities.length,
                        itemBuilder: (context, index) => ListTile(
                          title: Text(widget._suggestedCities[index]),
                        ),
                      ),
              ),
            ));
  }
}
