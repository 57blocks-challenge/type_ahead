import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:type_ahead/application/city_finder/city_finder_bloc.dart';
import 'package:type_ahead/infrastructure/cities/city_repository.dart';
import 'package:type_ahead/presentation/core/widgets/type_ahead_form_field.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: BlocProvider(
          create: (context) =>
              CityFinderBloc(cityRepository: new CityRepository()),
          child: MyHomePage()),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Type Ahead"),
      ),
      body: Column(
        children: <Widget>[
          BlocBuilder<CityFinderBloc, CityFinderState>(
            builder: (context, state) => TypeAheadFormField(
              labelText: "City",
              showOverlay: state.showOverlay,
              isLoading: state.isLoading,
              suggestedCities: state.suggestedCities,
              onTextChanged: (text) => BlocProvider.of<CityFinderBloc>(context)
                  .add(CityTextChanged(text)),
            ),
          ),
        ],
      ),
    );
  }
}
